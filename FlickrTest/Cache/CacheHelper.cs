﻿using System;
using System.Web;

namespace FlickrTest.Cache
{
    public static class CacheHelper
    {
        /// <summary>
        /// Simple cache helper
        /// </summary>
        /// <param name="key">The cache key used to reference the item.</param>
        /// <param name="function">The underlying method that referes to the object to be stored in the cache.</param>
        /// <returns>The item</returns>
        public static T Get<T>(string key, Func<T> function)
        {
            bool isLoadedFromCache;
            var obj = Get<T>(key, out isLoadedFromCache);

            if (isLoadedFromCache) return obj;

            obj = function.Invoke();
            HttpContext.Current.Cache.Add(key, obj, null, DateTime.Now.AddMinutes(3), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.Normal, null);

            return obj;
        }

        public static T Get<T>(string key, out bool isLoadedFromCache)
        {
            var obj = HttpContext.Current.Cache.Get(key);
            isLoadedFromCache = obj != null;
            return (!isLoadedFromCache && typeof(T).IsValueType) ? default(T) : (T)obj;
        }

        public static T Get<T>(string key)
        {
            bool isLoadedFromCache;
            return Get<T>(key, out isLoadedFromCache);
        }

        public static bool ContainsKey(string key)
        {
            return HttpContext.Current.Cache[key] != null;
        }
    }
}