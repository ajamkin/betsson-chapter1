﻿$(document).ready(function() {
    var tags = null;
    var $container = $('#container');
    var $tags = $('#tags');
    var $progressBar = $('#progressBar');

    $tags.keyup(function () {
        clearTimeout(tags);
        tags = setTimeout(function () {
            if ($tags.val().length < 3) {
                return false;
            }

            $progressBar.fadeIn();
            $.post(window.location + '/Home/GetImages', { tags: $tags.val() }, function(data) {
                $container.empty();

                var $row = $('<div class="row"/>');
                $.each(data, function(idx, item) {
                    if (idx % 3 == 0 && idx > 2) {
                        $row = $('<div class="row"/>');
                    }

                    var $item = $('<div class="col-md-4 flickr-image-container"/>');
                    var $image = $("<img>",
                    {
                        'class': 'has-tooltip',
                        title: item.IsFromCache ? 'Loaded from cache' : 'Loaded from flickr',
                        src: item.ImageUrl
                    });

                    $image.on('load', function() {
                        $item.append($image.hide().fadeIn());
                    });

                    $row.append($item);
                    $container.append($row);
                });
            }).always(function() {
                $progressBar.fadeOut();
            });

        }, 250);
    });


    $('body').tooltip({
        selector: '.has-tooltip'
    });
});
