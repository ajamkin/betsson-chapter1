using FlickrTest.Repository;
using StructureMap;

namespace FlickrTest.DependencyResolution
{
    public static class IoC
    {
        public static IContainer Initialize()
        {
            IContainer container = new Container(x => 
                x.For<IRepository>().Use<FlickrCacheableRepository>()
            );

            return container;
        }
    }
}